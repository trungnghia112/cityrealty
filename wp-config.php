<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'cityrealty');

/** MySQL database username */
define('DB_USER', 'root');

/** MySQL database password */
define('DB_PASSWORD', 'root');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8mb4');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         '])_88rPBn=^f2Ml@6=K$*te|i?g<#$Q-` f:=cflA{2s59lcg}`VF}ey|=1v2EmL');
define('SECURE_AUTH_KEY',  '9F-M^M(i|.+d(sIVqEpW`rAru.B*@m#TB+?$i-s.E5)^-pTj/U:2l&=F 2uD6!!f');
define('LOGGED_IN_KEY',    '9s1Nx~ R<u{a$:Z9(T<3~_$P9LzjbC!gc3cod&L6]]D$n/|[~uO`4Xv7dz%@96u-');
define('NONCE_KEY',        'XTF2^`:@1*M+O(+^{s%W)GK IxwTPkA25[%`DS&^Mki e*Ae}>br}!dHu[Q6u$@O');
define('AUTH_SALT',        'cdo|wc`vQQQP0O;MLCHljC]a_})Mg11HgdfXYYZ1i<=RzG|?)lX5>JSs/,4$r@s+');
define('SECURE_AUTH_SALT', 'Dd2Ay!r@.m_u1+?KNs3(_[Z&c?o Hn00R=67@uFd;@vfw?Q0mG^-)X@;$m{&s<HQ');
define('LOGGED_IN_SALT',   'J{ *N&_9P<AG@ZVEgo<>49=g,&^2J:}ik~$U|!rTN+}mp>U}l1HWyrto88{svf=c');
define('NONCE_SALT',       't-+pa*uP9Xw|[Vyna04A,VlTT ZFK)HkVnqJG:K39L4nPKf^N@862S/?CS8l8V`z');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');
