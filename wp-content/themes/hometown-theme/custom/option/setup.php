<?php 
	
// Option
$options = array(
	
	// Background
	array(
		'title' 	=> __('Setup', 'theme_admin'),
		'options' 	=> array(
			
			array(
				'type' 			=> 'content',
				'id' 			=> 'plugin',
				'title' 		=> __('1. Install Required Plugins', 'theme_admin'),
				'description' 	=> '',
				'default'	=> '',
				'value'	=> __('Click <a href="'.admin_url('themes.php?page=install-required-plugins').'">here</a> to install required plugins.', 'theme_admin')
			),
			array(
				'type' 			=> 'import_content',
				'id' 			=> 'import_demo_content',
				'title' 		=> __('2. Import Demo Content', 'theme_admin'),
				'description' 	=> __('<br />must <strong>activate all required plugin</strong> before import demo content<br /><br />recommended to perform on fresh installation<br /><br />all content will be deleted', 'theme_admin'),
				'options' 		=> array(
					'demo-1' 	=> __('Demo #1', 'theme_admin'),
					'demo-2' 	=> __('Demo #2', 'theme_admin'),
				)
			),
			
					
		)
	),
	
);

$config = array(
	'title'			=> __('Setup', 'theme_admin'),
	'group_id' 		=> 'setup',
	'active_first' 	=> true
);
	
	
return array( 'options' => $options, 'config' => $config );

?>