<?php

add_action( 'wp_ajax_nt_ajax_save_option', 'nt_ajax_save_option' );
function nt_ajax_save_option() {
	wp_parse_str( stripslashes( $_REQUEST['options'] ), $options);

	update_option(THEME_SLUG . '_options', $options);

	$result = array('result' => 'ok');
	echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
	die();
}

add_action( 'wp_ajax_nt_ajax_generate_stack_option', 'nt_ajax_generate_stack_option' );
function nt_ajax_generate_stack_option() {
	$stack_option = json_decode(stripslashes($_POST['option']),true);
	$stack_config = json_decode(stripslashes($_POST['config']),true);
	$stack_subgroup = json_decode(stripslashes($_POST['subgroup']),true);
	if(!is_array($stack_subgroup)){
		$stack_subgroup = array();
	}
	$stack_subgroup[] = $_POST['stack_id'];

	// modify option id, toggle, toggle_group
	$extend = '-' . $_POST['stack_id'];
	$stack_config['stack_id'] = $_POST['stack_id'];
	$stack_config['subgroup'] = $stack_subgroup;

	// generate stack option
	$nt_input_tool = new nt_input_tool($stack_option, $stack_config);
	$nt_input_tool->generate_stack_option();

	die();
}

// nt_do_shortcode
add_action( 'wp_ajax_nt_do_shortcode', 'nt_do_shortcode' );
function nt_do_shortcode() {
	$result = array('result' => do_shortcode($_REQUEST['content']));
	echo json_encode($result);
	die();
}

add_action( 'wp_ajax_nt_ajax_import_content', 'nt_ajax_import_content' );
function nt_ajax_import_content() {
	define('DOING_AJAX_IMPORT', true);

	$msg = '';
	$options = $_REQUEST['options'];

	if(!$options) {
		$msg = 'Please select demo content';
		$result = array('result' => 'fail', 'msg' => $msg);
		echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
		die();
	}
	
	if ( !class_exists("WP_Import") ) {
		define("WP_LOAD_IMPORTERS", true);
		require_once( THEME_FRAMEWORK_DIR . '/lib/wordpress-importer/wordpress-importer.php' );
	}
	require_once( THEME_FUNCTIONS_DIR . '/base-importer.php' );

	$demo_file = '';
	if(isset($options)) {
		$demo_file = THEME_CUSTOM_DIR . '/demo/'.$options.'/demo.xml';
	}
	$importer = new Base_Importer();

	// Clear all post
	$posts = get_posts(array('post_status' => array('any', 'trash'), 'posts_per_page' => -1, 'post_type' => 'wpcf7_contact_form'));
	foreach($posts as $post) {
		wp_delete_post($post->ID, true);
	}
	$posts = get_posts(array('post_status' => array('any', 'trash'), 'posts_per_page' => -1, 'post_type' => 'revision'));
	foreach($posts as $post) {
		wp_delete_post($post->ID, true);
	}
	$posts = get_posts(array('post_status' => array('any', 'trash'), 'posts_per_page' => -1, 'post_type' => 'nav_menu_item'));
	foreach($posts as $post) {
		wp_delete_post($post->ID, true);
	}
	$posts = get_posts(array('post_status' => array('any', 'trash'), 'posts_per_page' => -1, 'post_type' => 'any'));
	foreach($posts as $post) {
		wp_delete_post($post->ID, true);
	}
	ob_start();
	$importer->import( $demo_file, $options);
	$import_message = ob_get_contents();
	ob_end_clean();

	$result = array('result' => 'ok', 'msg' => $msg);
	echo htmlspecialchars(json_encode($result), ENT_NOQUOTES);
	die();
}