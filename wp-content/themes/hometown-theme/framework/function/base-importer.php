<?php
class Base_Import extends WP_Import{
	public $file;
	public $demo_group;
	
	public function process_attachment( $post, $url ) {
		$post_id = $post["import_id"];
		$xml = simplexml_load_file($this->file);
		$result = $xml->xpath("channel/wg_custom_attachment_url/attachment[post_id='$post_id']/custom_url");
		$custom_url = ($result!=false) ? $result[0] : '';

		if ($custom_url != '') {
			$url = THEME_CUSTOM_URI . '/demo/' . $custom_url;
		}
		
		$return = parent::process_attachment( $post, $url );
		return $return;
	}

	public function import($file) {
		$this->file = $file;
		return parent::import($file);
	}

	public function process_menu_item( $item ) {

		// skip draft, orphaned menu items
		if ( 'draft' == $item['status'] )
			return;

		$menu_slug = false;
		if ( isset($item['terms']) ) {
			// loop through terms, assume first nav_menu term is correct menu
			foreach ( $item['terms'] as $term ) {
				if ( 'nav_menu' == $term['domain'] ) {
					$menu_slug = $term['slug'];
					break;
				}
			}
		}

		// no nav_menu term associated with this menu item
		if ( ! $menu_slug ) {
			_e( 'Menu item skipped due to missing menu slug', 'wordpress-importer' );
			echo '<br />';
			return;
		}

		$menu_id = term_exists( $menu_slug, 'nav_menu' );
		if ( ! $menu_id ) {
			printf( __( 'Menu item skipped due to invalid menu slug: %s', 'wordpress-importer' ), esc_html( $menu_slug ) );
			echo '<br />';
			return;
		} else {
			$menu_id = is_array( $menu_id ) ? $menu_id['term_id'] : $menu_id;
		}

		foreach ( $item['postmeta'] as $meta )
			$$meta['key'] = $meta['value'];

		if ( 'taxonomy' == $_menu_item_type && isset( $this->processed_terms[intval($_menu_item_object_id)] ) ) {
			$_menu_item_object_id = $this->processed_terms[intval($_menu_item_object_id)];
		} else if ( 'post_type' == $_menu_item_type && isset( $this->processed_posts[intval($_menu_item_object_id)] ) ) {
			$_menu_item_object_id = $this->processed_posts[intval($_menu_item_object_id)];
		} else if ( 'custom' != $_menu_item_type ) {
			// associated object is missing or not imported yet, we'll retry later
			$this->missing_menu_items[] = $item;
			return;
		}

		if ( isset( $this->processed_menu_items[intval($_menu_item_menu_item_parent)] ) ) {
			$_menu_item_menu_item_parent = $this->processed_menu_items[intval($_menu_item_menu_item_parent)];
		} else if ( $_menu_item_menu_item_parent ) {
			$this->menu_item_orphans[intval($item['post_id'])] = (int) $_menu_item_menu_item_parent;
			$_menu_item_menu_item_parent = 0;
		}

		// wp_update_nav_menu_item expects CSS classes as a space separated string
		$_menu_item_classes = maybe_unserialize( $_menu_item_classes );
		if ( is_array( $_menu_item_classes ) )
			$_menu_item_classes = implode( ' ', $_menu_item_classes );

		$args = array(
			'menu-item-object-id' => $_menu_item_object_id,
			'menu-item-object' => $_menu_item_object,
			'menu-item-parent-id' => $_menu_item_menu_item_parent,
			'menu-item-position' => intval( $item['menu_order'] ),
			'menu-item-type' => $_menu_item_type,
			'menu-item-title' => $item['post_title'],
			'menu-item-url' => $_menu_item_url,
			'menu-item-description' => $item['post_content'],
			'menu-item-attr-title' => $item['post_excerpt'],
			'menu-item-target' => $_menu_item_target,
			'menu-item-classes' => $_menu_item_classes,
			'menu-item-xfn' => $_menu_item_xfn,
			'menu-item-status' => $item['status']
		);

		foreach ( $item['postmeta'] as $meta ) {
			if( $meta['key'][0] != '_' )
				$p_meta[$meta['key']] = $meta['value'];
		}
		$args = wp_parse_args($args, $p_meta);

		$id = wp_update_nav_menu_item( $menu_id, 0, $args );
		if ( $id && ! is_wp_error( $id ) )
			$this->processed_menu_items[intval($item['post_id'])] = (int) $id;
	}
}
class Base_Importer {
	protected $importer;
	protected $demo;
	protected $success;
	protected $messages;

	public function __construct() {
		$this->importer = new Base_Import();
		$this->importer->fetch_attachments = true;
	}

	public function importWidgets($config) {
		if ( @$config['sidebar'] ) {
			update_option( 'sidebars_widgets', $config['sidebar'] );
		}

		if ( @$config['options'] ) {
			foreach ($config['options'] as $name => $value) {
				update_option( "widget_$name", maybe_unserialize($value) );
			}
		}
	}

	public function importThemeOptions($options) {
		update_option(THEME_SLUG . '_options', $options);
	}

	public function import($demo_file, $demo_group) {

		$this->demo = $demo_file;
		$this->success = false;
		$this->messages = '';

		set_time_limit(0);
		ob_start();

		add_action('import_end',array(&$this,'import_end'));
		$this->importer->demo_group = $demo_group;
		$this->importer->import($this->demo);

		return $this->success;
	}

	// Hook the WP_Import when importing done
	public function import_end() {
		$this->messages = ob_get_contents();
		$this->success = true;
		ob_end_clean();

		// Exact the custom export string
		$raw = file_get_contents( $this->demo );
		$openTag = Base_Export::OPEN_TAG;
		$closeTag = Base_Export::CLOSE_TAG;
		$openTagLength = strlen( $openTag );
		$startIndex = strpos( $raw, $openTag );
		$endIndex = ( $startIndex !== false ) ? strpos( $raw, $closeTag, $startIndex ) : false;
		$customExportText = ( $endIndex !== false ) ? substr( $raw, $startIndex + $openTagLength, $endIndex - $startIndex - $openTagLength) : '';
		
		if ($customExportText) {
			$data =  @unserialize( base64_decode( $customExportText ) );
			
			// Import Demo Theme Options 
			if ( @$data['theme_options'] ) {
				$this->importThemeOptions($data['theme_options']);
			}

			// Import Demo Widgets
			if ( @is_array($data['widgets']) ) {
				$this->importWidgets($data['widgets']);
			}
			
			// Set navigation menu
			$this->autosetThemeLocation();

			// Set static home & blog
			update_option( 'show_on_front', 'page' );
			$home = get_page_by_title( 'Home' );
			update_option( 'page_on_front', $home->ID );
			$blog = get_page_by_title( 'Blog' );
   			update_option( 'page_for_posts', $blog->ID );
		}
	}

	private function autosetThemeLocation() {
		$menu_locations = array();
		$nav_menus = get_terms('nav_menu');
		foreach($nav_menus as $menu) {
			$menu_locations[$menu->slug] = $menu->term_id;
		}
		set_theme_mod('nav_menu_locations', $menu_locations);
	}
}
